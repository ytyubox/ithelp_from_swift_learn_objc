//
//  Math.h
//  Core
//
//  Created by 游宗諭 on 2020/7/11.
//  Copyright © 2020 ytyubox. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyMath : NSObject
+ (NSString*) identifier;

- (int) toAdd:(int) lValue
            r:(int) rValue;
@end

NS_ASSUME_NONNULL_END
