//
//  Core.h
//  Core
//
//  Created by 游宗諭 on 2020/7/11.
//

#import <Foundation/Foundation.h>

//! Project version number for Core.
FOUNDATION_EXPORT double CoreVersionNumber;

//! Project version string for Core.
FOUNDATION_EXPORT const unsigned char CoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Core/PublicHeader.h>
#import <Core/MyMath.h>
